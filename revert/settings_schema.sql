-- Revert aprt:settings_schema from pg
BEGIN;

-- drop settings schema and everything in it
DROP SCHEMA IF EXISTS settings CASCADE;

COMMIT;
