-- Verify aprt:settings_schema on pg
BEGIN;

DO $$
BEGIN
    PERFORM settings.set('key', 'value');
    ASSERT settings.get('key') = 'value';
    PERFORM settings.set('key', 'anothervalue');
    ASSERT settings.get('key') = 'anothervalue';
END $$;

ROLLBACK;
